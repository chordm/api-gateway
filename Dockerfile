FROM node:16-alpine as builder
ENV NODE_ENV build
WORKDIR /usr/app
COPY . /usr/app
RUN yarn install --frozen-lockfile --production --ignore-optional
RUN yarn build

FROM keymetrics/pm2:16-alpine
ENV NODE_ENV production
USER node
WORKDIR /usr/app
COPY --from=builder /usr/app/dist/ /usr/app/dist/

CMD [ "pm2-runtime", "start", "npm run start:prod" ]
