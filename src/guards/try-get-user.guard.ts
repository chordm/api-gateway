import { Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { AuthGuard } from '@nestjs/passport';

@Injectable()
export default class TryGetUserGuard extends AuthGuard('jwt') {
  handleRequest(err, user, info, context) {
    return user ?? true;
  }
}
