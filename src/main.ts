import { ValidationPipe } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './modules/app/app.module';
import * as cookieParser from 'cookie-parser';
import helmet from 'helmet';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const configService = app.get(ConfigService);
  const port = configService.get('PORT');
  const enabledOrigin = configService.get('ENABLED_INBOUND_ORIGIN');

  app.setGlobalPrefix('api');
  app.useGlobalPipes(new ValidationPipe());
  app.use(cookieParser());
  app.use(helmet());

  app.enableCors({
    // Added in dev purposes
    // TODO: remove it in prod
    origin: [enabledOrigin],
    credentials: true,
  });

  await app.listen(port ?? 3000);
}

bootstrap();
