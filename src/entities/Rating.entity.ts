import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  ManyToOne,
} from 'typeorm';
import { Song } from './Song.entity';
import { User } from './User.entity';

@Entity()
export class Rating {
  @PrimaryGeneratedColumn()
  id: string;

  @Column({ type: 'integer', nullable: false })
  value: number;

  @CreateDateColumn({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
  })
  createdAt: Date;

  @ManyToOne(
    () => Song,
    song => song.ratings,
  )
  song: Song;

  @ManyToOne(
    () => User,
    user => user.ratedSongs,
  )
  user: User;
}
