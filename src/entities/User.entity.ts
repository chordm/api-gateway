import { Entity, Column, PrimaryColumn, OneToMany } from 'typeorm';
import { Rating } from './Rating.entity';
import { Song } from './Song.entity';

@Entity()
export class User {
  @PrimaryColumn({ type: 'varchar', length: 255 })
  id: string;

  @Column({ type: 'varchar', length: 255, nullable: false })
  username: string;

  @Column({ type: 'varchar', length: 1024, nullable: true })
  picture?: string;

  @Column({ type: 'varchar', length: 255, nullable: false })
  email: string;

  @Column({ type: 'varchar', length: 255, nullable: true })
  locale?: string;

  @OneToMany(
    () => Song,
    song => song.author,
    { nullable: false, onDelete: 'CASCADE' },
  )
  authoredSongs: Song[];

  @OneToMany(
    () => Rating,
    rating => rating.user,
    { nullable: false, onDelete: 'CASCADE' },
  )
  ratedSongs: Song[];
}
