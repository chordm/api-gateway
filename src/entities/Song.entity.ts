import { ChordPanels, RhythmPanels } from 'types/Song.type';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  ManyToOne,
  OneToMany,
} from 'typeorm';
import { Performer } from './Performer.entity';
import { User } from './User.entity';
import { Rating } from './Rating.entity';

@Entity()
export class Song {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    type: 'jsonb',
    array: false,
    nullable: false,
  })
  rhythmPanels: RhythmPanels;

  @Column({
    type: 'jsonb',
    array: false,
    nullable: false,
  })
  chordPanels: ChordPanels;

  @Column({
    type: 'jsonb',
    array: false,
    nullable: false,
  })
  titles: string[];

  @Column({
    type: 'jsonb',
    array: false,
    nullable: false,
  })
  rhythm: [number, number];

  @Column({ type: 'varchar', length: 255, nullable: false })
  songTitle: string;

  @Column({ type: 'int' })
  views: number;

  @ManyToOne(
    () => Performer,
    performer => performer.authoredSongs,
  )
  performer;

  @ManyToOne(
    () => User,
    user => user.authoredSongs,
  )
  author: User;

  @OneToMany(
    () => Rating,
    rating => rating.song,
  )
  ratings: Rating[];
}
