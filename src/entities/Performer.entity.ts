import { Entity, Column, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Song } from './Song.entity';

@Entity()
export class Performer {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar', length: 255, nullable: true, unique: true })
  bandName: string;

  @OneToMany(
    type => Song,
    song => song.performer,
    { nullable: false, onDelete: 'CASCADE' },
  )
  authoredSongs: Song[];
}
