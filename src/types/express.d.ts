type AuthUserDto = import('../modules/auth/auth.types').AuthUserDto;

declare namespace Express {
  // eslint-disable-next-line @typescript-eslint/no-empty-interface
  export interface User extends AuthUserDto {}
}
