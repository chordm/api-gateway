/**
 * es - ♭
 * is - #
 * dur - major
 * moll - minor
 */

// TODO: Move same types on BE and FE to separate project

export enum ChordKeys {
  A = 'A',
  B = 'B',
  C = 'C',
  D = 'D',
  E = 'E',
  F = 'F',
  G = 'G',
}

export interface Chord {
  key: ChordKeys;
  body?: string;
  tonic?: ChordKeys;
}

export type ChordAlign = {
  lineIndex: number;
  tactIndex: number;
  chordIndex: number;
};

export type ChordItem = {
  word: string;
  chordAlign: ChordAlign;
  id: string;
};

export type ChordString = ChordItem[];
export type ChordPanel = ChordString[];
export type ChordPanels = ChordPanel[];

export type RhythmTact = (Chord | null)[];
export type RhythmLine = RhythmTact[];
export type RhythmPanel = RhythmLine[];
export type RhythmPanels = RhythmPanel[];
