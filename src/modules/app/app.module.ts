import { Module } from '@nestjs/common';
import { UserModule } from '../user/user.module';
import { ConfigModule } from '@nestjs/config';
import { AuthModule } from '../auth/auth.module';
import * as Joi from '@hapi/joi';
import { DatabaseModule } from '../database/database.module';
import { SongModule } from '../song/song.module';
import { PerformerModule } from 'modules/performer/performer.module';
import { RatingModule } from 'modules/rating/rating.module';
import { SearchModule } from 'modules/search/search.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      validationSchema: Joi.object({
        GOOGLE_CLIENT_ID: Joi.string().required(),
        GOOGLE_SECRET: Joi.string().required(),
        GOOGLE_REDIRECT_URL: Joi.string().required(),
        PORT: Joi.number(),
        POSTGRES_HOST: Joi.string().required(),
        POSTGRES_PORT: Joi.number().required(),
        POSTGRES_USER: Joi.string().required(),
        POSTGRES_PASSWORD: Joi.string().required(),
        POSTGRES_DB: Joi.string().required(),
        JWT_SECRET: Joi.string().required(),
        JWT_EXPIRATION_TIME: Joi.number().required(),
        ENABLED_INBOUND_ORIGIN: Joi.string().required(),
        CLIENT_URL: Joi.string().required(),
      }),
    }),
    DatabaseModule,
    AuthModule,
    UserModule,
    SongModule,
    PerformerModule,
    RatingModule,
    SearchModule,
  ],
})
export class AppModule {}
