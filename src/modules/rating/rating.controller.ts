import {
  Body,
  Controller,
  Get,
  Param,
  ParseIntPipe,
  Put,
  Req,
  UseGuards,
  UsePipes,
} from '@nestjs/common';
import { Request } from 'express';
import JwtAuthenticationGuard from 'guards/jwt-authentication.guard';
import { JoiValidationPipe } from 'pipes/JoiValidation.pipe';
import { RatingService } from './rating.service';
import { CreateRatingDto } from './rating.types';
import { createRatingSchema } from './rating.validator';

@Controller('rating')
export class RatingController {
  constructor(private readonly ratingService: RatingService) {}

  @Put()
  @UseGuards(JwtAuthenticationGuard)
  @UsePipes(new JoiValidationPipe(createRatingSchema))
  async createOne(
    @Body() createRatingDto: CreateRatingDto,
    @Req() request: Request,
  ) {
    return this.ratingService.createOne(createRatingDto, request.user.id);
  }

  @Get('/:songId')
  async getRatingForSong(
    @Param('songId', ParseIntPipe) songId: number,
    @Req() request: Request,
  ) {
    return this.ratingService.getRatingForSong(songId, request.user.id);
  }
}
