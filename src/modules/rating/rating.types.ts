import { IsNotEmpty, IsNumber } from 'class-validator';

export class CreateRatingDto {
  @IsNumber()
  @IsNotEmpty()
  value: number;

  @IsNumber()
  @IsNotEmpty()
  songId: number;
}
