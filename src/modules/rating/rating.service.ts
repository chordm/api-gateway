import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Rating } from 'entities/Rating.entity';
import { Song } from 'entities/Song.entity';
import { User } from 'entities/User.entity';
import { Repository } from 'typeorm';
import { CreateRatingDto } from './rating.types';

@Injectable()
export class RatingService {
  constructor(
    @InjectRepository(Song) private readonly songRepository: Repository<Song>,
    @InjectRepository(User) private readonly userRepository: Repository<User>,
    @InjectRepository(Rating)
    private readonly ratingRepository: Repository<Rating>,
  ) {}

  private sanitizeRating(rating: Rating) {
    const { user, song, ...ratingDto } = rating;
    return ratingDto;
  }

  public async createOne(createRatingDto: CreateRatingDto, userId: string) {
    const alreadyCreatedRatingQuery = this.ratingRepository
      .createQueryBuilder('rating')
      .leftJoinAndSelect('rating.user', 'user')
      .where('user.id = :userId', { userId })
      .leftJoinAndSelect('rating.song', 'song')
      .andWhere('song.id = :songId', { songId: createRatingDto.songId })
      .getOne();

    const [song, user, alreadyCreatedRating] = await Promise.all([
      this.songRepository.findOne({
        where: { id: createRatingDto.songId },
      }),
      this.userRepository.findOne({
        where: { id: userId },
      }),
      alreadyCreatedRatingQuery,
    ]);

    if (!song) {
      throw new HttpException('Song not found', HttpStatus.NOT_FOUND);
    }
    if (alreadyCreatedRating) {
      alreadyCreatedRating.value = createRatingDto.value;
      const savedRating =
        await this.ratingRepository.save(alreadyCreatedRating);
      return this.sanitizeRating(savedRating);
    }

    const rating = new Rating();

    rating.song = song;
    rating.value = createRatingDto.value;
    rating.user = user;

    const savedRating = await this.ratingRepository.save(rating);

    return this.sanitizeRating(savedRating);
  }

  public async getRatingForSong(songId: number, userId: string) {
    const [averageRating, current] = await Promise.all([
      this.songRepository
        .createQueryBuilder('song')
        .innerJoin('song.ratings', 'rating')
        .select('AVG(rating.value)::numeric(10, 1)', 'average')
        .where('song.id = :songId', { songId })
        .groupBy('song.id')
        .getRawOne(),
      this.ratingRepository
        .createQueryBuilder('rating')
        .innerJoin('rating.song', 'song')
        .innerJoin('rating.user', 'user')
        .where('song.id = :songId', { songId })
        .andWhere('user.id = :userId', { userId })
        .getOne(),
    ]);

    return {
      average: averageRating?.average ?? null,
      current: current ?? null,
    };
  }
}
