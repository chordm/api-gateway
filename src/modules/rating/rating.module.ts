import { Module } from '@nestjs/common';
import { RatingService } from './rating.service';
import { RatingController } from './rating.controller';
import { Song } from 'entities/Song.entity';
import { Rating } from 'entities/Rating.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from 'entities/User.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Song, Rating, User])],
  providers: [RatingService],
  controllers: [RatingController],
})
export class RatingModule {}
