import * as Joi from '@hapi/joi';

export const createRatingSchema = Joi.object().keys({
  value: Joi.number()
    .min(1)
    .max(5)
    .required(),
  songId: Joi.number().required(),
});
