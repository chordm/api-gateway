import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Performer } from 'entities/Performer.entity';
import { Song } from 'entities/Song.entity';
import { SearchController } from './search.controller';
import { SearchService } from './search.service';

@Module({
  imports: [TypeOrmModule.forFeature([Performer, Song])],
  controllers: [SearchController],
  providers: [SearchService],
})
export class SearchModule {}
