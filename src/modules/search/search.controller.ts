import { Controller, DefaultValuePipe, Get, Query } from '@nestjs/common';
import { SearchService } from './search.service';

@Controller('search')
export class SearchController {
  constructor(private readonly searchService: SearchService) {}

  @Get('global')
  async globalSearch(
    @Query('filter', new DefaultValuePipe('')) filter: string,
  ) {
    return this.searchService.globalSearch({ filter });
  }
}
