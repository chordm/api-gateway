import { Performer } from 'entities/Performer.entity';
import { Song } from 'entities/Song.entity';

export interface GlobalSearchDto {
  songs: {
    id: Song['id'];
    songTitle: Song['songTitle'];
    views: Song['views'];
    performer: {
      id: Performer['id'];
      bandName: Performer['bandName'];
    };
  }[];
  performers: {
    id: Performer['id'];
    bandName: Performer['bandName'];
    views: number;
  }[];
}
