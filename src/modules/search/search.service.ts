import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Performer } from 'entities/Performer.entity';
import { Song } from 'entities/Song.entity';
import { Repository } from 'typeorm';
import { GlobalSearchDto } from './search.dto';

@Injectable()
export class SearchService {
  static GLOBAL_SEARCH_LIMIT = 5;

  constructor(
    @InjectRepository(Performer)
    private readonly performerRepository: Repository<Performer>,
    @InjectRepository(Song)
    private readonly songRepository: Repository<Song>,
  ) {}

  async globalSearch(options: { filter: string }): Promise<GlobalSearchDto> {
    const searchParts = options.filter
      .trim()
      .split(/(\s*-\s*)/)
      .filter(s => /\w+/.test(s))
      .slice(0, 2);

    const songsQuery = this.songRepository
      .createQueryBuilder('song')
      .select('song.songTitle', 'songTitle')
      .addSelect('song.id', 'id')
      .innerJoinAndSelect('song.performer', 'performer')
      .addSelect('song.views', 'views')
      .limit(SearchService.GLOBAL_SEARCH_LIMIT)
      .orderBy('views', 'DESC', 'NULLS LAST');

    const performerQuery = this.performerRepository
      .createQueryBuilder('performer')
      .innerJoin('performer.authoredSongs', 'song')
      .addSelect('SUM(DISTINCT song.views)::integer', 'views')
      .addSelect('performer.bandName', 'bandName')
      .groupBy('performer.id')
      .limit(SearchService.GLOBAL_SEARCH_LIMIT)
      .orderBy('views', 'DESC', 'NULLS LAST');

    let songsPromise: Promise<(Pick<Song, 'songTitle' | 'views' | 'id'> & {
      performer_id: Performer['id'];
      performer_bandName: Performer['bandName'];
    })[]> = Promise.resolve([]);
    let performersPromise: Promise<(Pick<Performer, 'bandName'> & {
      performer_id: Performer['id'];
      views: number;
    })[]> = Promise.resolve([]);

    if (searchParts.length === 1) {
      songsQuery.where('song.songTitle ILIKE :filter', {
        filter: `%${searchParts[0]}%`,
      });
      performerQuery.where('performer.bandName ILIKE :filter', {
        filter: `%${searchParts[0]}%`,
      });

      songsPromise = songsQuery.getRawMany();
      performersPromise = performerQuery.getRawMany();
    } else if (searchParts.length === 2) {
      songsQuery
        .where(
          '("song"."songTitle" ILIKE :leftPart AND "performer"."bandName" ILIKE :rightPart)',
        )
        .orWhere(
          '("song"."songTitle" ILIKE :rightPart AND "performer"."bandName" ILIKE :leftPart)',
        )
        .setParameters({
          leftPart: `%${searchParts[0]}%`,
          rightPart: `%${searchParts[1]}%`,
        });

      songsPromise = songsQuery.getRawMany();
    }

    const [rawSongs, rawPerformers] = await Promise.all([
      songsPromise,
      performersPromise,
    ]);

    const songs = rawSongs.map(
      ({ performer_bandName, performer_id, ...rest }) => ({
        ...rest,
        performer: {
          id: performer_id,
          bandName: performer_bandName,
        },
      }),
    );

    const performers = rawPerformers.map(({ performer_id, ...rest }) => ({
      ...rest,
      id: performer_id,
    }));

    return { songs, performers };
  }
}
