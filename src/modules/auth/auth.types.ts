import { IsEmail, IsString, IsNotEmpty } from 'class-validator';
export class AuthUserDto {
  @IsString()
  @IsNotEmpty()
  id: string;


  @IsString()
  @IsNotEmpty()
  username: string;

  @IsString()
  @IsNotEmpty()
  picture: string;

  @IsEmail()
  email: string;
  
  @IsString()
  @IsNotEmpty()
  accessToken: string;

  @IsString()
  @IsNotEmpty()
  locale: string;
}

export interface TokenPayload {
  userId: string;
}
