import { PassportStrategy } from '@nestjs/passport';
import { Strategy } from 'passport-google-oauth20';
import { Injectable } from '@nestjs/common';
import { AuthUserDto } from 'modules/auth/auth.types';

@Injectable()
export class GoogleStrategy extends PassportStrategy(Strategy, 'google') {
  constructor() {
    super({
      clientID: process.env.GOOGLE_CLIENT_ID,
      clientSecret: process.env.GOOGLE_SECRET,
      callbackURL: process.env.GOOGLE_REDIRECT_URL,
      scope: ['email', 'profile', 'openid'],
    });
  }

  async validate(
    accessToken: string,
    refreshToken: string,
    profile,
  ): Promise<AuthUserDto> {
    const { emails, photos, displayName, id, _json } = profile;

    return {
      email: emails[0].value,
      username: displayName,
      picture: photos[0].value,
      id,
      accessToken,
      locale: _json.locale,
    };
  }
}
