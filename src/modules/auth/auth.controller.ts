import {
  Controller,
  Get,
  Header,
  HttpCode,
  Post,
  Req,
  Res,
  UseGuards,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Request, Response } from 'express';
import GoogleAuthenticationGuard from 'guards/google-authentication.guard';
import JwtAuthenticationGuard from 'guards/jwt-authentication.guard';
import { AuthService } from './auth.service';

@Controller('auth')
export class AuthController {
  constructor(
    private readonly authService: AuthService,
    private readonly configService: ConfigService,
  ) {}

  @HttpCode(200)
  @Get('google/redirect')
  @UseGuards(GoogleAuthenticationGuard)
  async googleAuthRedirect(@Req() req: Request, @Res() response: Response) {
    const user = await this.authService.googleLogin(req);
    const cookie = this.authService.getCookieWithJwtToken(user.id);
    response.setHeader('Set-Cookie', cookie);
    response.redirect(this.configService.get('CLIENT_URL'));
  }

  @UseGuards(JwtAuthenticationGuard)
  @Post('log-out')
  async logOut(@Res() response: Response) {
    response.setHeader('Set-Cookie', this.authService.getCookieForLogOut());
    return response.sendStatus(200);
  }

  @UseGuards(JwtAuthenticationGuard)
  @Get()
  @Header('Cache-Control', 'no-cache')
  @Header('Pragma', 'no-cache')
  @Header('Expires', '0')
  authenticate(@Req() request: Request) {
    return request.user;
  }
}
