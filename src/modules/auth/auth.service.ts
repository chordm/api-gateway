import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository } from '@nestjs/typeorm';
import { Request } from 'express';
import { User } from 'entities/User.entity';
import { Repository } from 'typeorm';
import { TokenPayload } from 'modules/auth/auth.types';
@Injectable()
export class AuthService {
  constructor(
    @InjectRepository(User)
    private usersRepository: Repository<User>,
    private readonly jwtService: JwtService,
    private readonly configService: ConfigService,
  ) {}

  public getCookieWithJwtToken(userId: string) {
    const payload: TokenPayload = { userId };
    const token = this.jwtService.sign(payload);
    return `\
      Authentication=${token};\
      HttpOnly;\
      Path=/;\
      Max-Age=${this.configService.get('JWT_EXPIRATION_TIME')}\
    `;
  }

  async googleLogin({ user }: Request) {
    if (!user) {
      throw new HttpException('No user from google', HttpStatus.NOT_FOUND);
    }

    const existingUser = await this.usersRepository.findOne({
      where: { id: user.id },
    });

    if (!existingUser) {
      const newUser = new User();
      newUser.id = user.id;
      newUser.username = user.username;
      newUser.email = user.email;
      newUser.picture = user.picture;
      newUser.locale = user.locale;
      await this.usersRepository.save(newUser);
    }

    return user;
  }

  public getCookieForLogOut() {
    return `Authentication=; HttpOnly; Path=/; Max-Age=0`;
  }
}
