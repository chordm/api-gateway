import {
  Controller,
  DefaultValuePipe,
  Get,
  ParseIntPipe,
  Query,
} from '@nestjs/common';
import { Performer } from 'entities/Performer.entity';
import { PerformerService } from './performer.service';

@Controller('performer')
export class PerformerController {
  constructor(private readonly performerService: PerformerService) {}

  @Get()
  async getSongs(
    @Query('top', new DefaultValuePipe(15), ParseIntPipe) top: number,
    @Query('skip', new DefaultValuePipe(0), ParseIntPipe) skip: number,
    @Query('filter', new DefaultValuePipe('')) filter: string,
    @Query('orderBy', new DefaultValuePipe('id')) orderBy: keyof Performer,
  ) {
    return this.performerService.getAll({ top, skip, filter, orderBy });
  }
}
