import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Performer } from 'entities/Performer.entity';
import { Song } from 'entities/Song.entity';
import { Repository } from 'typeorm';

@Injectable()
export class PerformerService {
  constructor(
    @InjectRepository(Performer)
    private readonly performerRepository: Repository<Performer>,
  ) {}

  async getAll(options: {
    top: number;
    skip: number;
    filter: string;
    orderBy: keyof Performer | 'views' | 'averageRating';
  }) {
    const orderByField =
      options.orderBy === 'averageRating' || options.orderBy === 'views'
        ? `"${options.orderBy}"`
        : `performer.${options.orderBy}`;
    const { raw } = await this.performerRepository
      .createQueryBuilder('performer')
      .addSelect('(COUNT(*) OVER())::INTEGER AS totalcount')
      .addSelect('SUM(DISTINCT song.views)::INTEGER', 'views')
      .addSelect('performer.bandName', 'bandName')
      .addSelect(query =>
        query
          .select('song.songTitle', 'highlightedSong')
          .from(Song, 'song')
          .where('"song"."performerId" = performer.id')
          .orderBy('song.views', 'DESC', 'NULLS LAST')
          .limit(1),
      )
      .innerJoin('performer.authoredSongs', 'song')
      .leftJoin('song.ratings', 'rating')
      .addSelect('AVG(rating.value)::numeric(10, 1)', 'averageRating')
      .groupBy('performer.id')
      .where('performer.bandName ILIKE :filter', {
        filter: `%${options.filter}%`,
      })
      .limit(options.top)
      .offset(options.skip)
      .orderBy(orderByField, 'DESC', 'NULLS LAST')
      .getRawAndEntities();

    const items = raw.map(({ totalcount, performer_id, ...rest }) => ({
      id: performer_id,
      ...rest,
    }));
    const totalCount = raw[0]?.totalcount ?? 0;

    return { items, totalCount };
  }
}
