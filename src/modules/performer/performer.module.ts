import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Performer } from 'entities/Performer.entity';
import { Song } from 'entities/Song.entity';
import { PerformerController } from './performer.controller';
import { PerformerService } from './performer.service';

@Module({
  imports: [TypeOrmModule.forFeature([Performer, Song])],
  controllers: [PerformerController],
  providers: [PerformerService],
})
export class PerformerModule {}
