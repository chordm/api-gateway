import {
  Body,
  Controller,
  DefaultValuePipe,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Put,
  Query,
  Req,
  UseGuards,
  UsePipes,
} from '@nestjs/common';
import { Song } from 'entities/Song.entity';
import { Request } from 'express';
import JwtAuthenticationGuard from 'guards/jwt-authentication.guard';
import { JoiValidationPipe } from 'pipes/JoiValidation.pipe';
import { CreateSongDto } from './song.type';
import { SongService } from './song.service';
import { createSongSchema } from './song.validator';

@Controller('song')
export class SongController {
  constructor(private readonly songService: SongService) {}

  @Post()
  @UseGuards(JwtAuthenticationGuard)
  @UsePipes(new JoiValidationPipe(createSongSchema))
  createSong(@Body() createSongDto: CreateSongDto, @Req() request: Request) {
    return this.songService.createSong(createSongDto, request.user);
  }

  @Get()
  getSongs(
    @Query('top', new DefaultValuePipe(15), ParseIntPipe) top: number,
    @Query('skip', new DefaultValuePipe(0), ParseIntPipe) skip: number,
    @Query('filter', new DefaultValuePipe('')) filter: string,
    @Query('orderBy', new DefaultValuePipe('id')) orderBy: keyof Song,
  ) {
    return this.songService.getAll({ top, skip, filter, orderBy });
  }

  @Get(':songId')
  getSong(@Param('songId') songId: number) {
    return this.songService.getById(songId);
  }

  @Get(':songId/variants')
  getSongVariants(@Param('songId') songId: number) {
    return this.songService.getSongVariants(songId);
  }

  @Put(':songId')
  updateSong(@Param('songId') songId: number, @Body() songDto: CreateSongDto) {
    return this.songService.updateById(songId, songDto);
  }

  @Put(':songId/views')
  addViews(
    @Param('songId') songId: number,
    @Query('amount', new DefaultValuePipe(1)) viewsAmount: number,
  ) {
    return this.songService.addViews(songId, viewsAmount);
  }
}
