import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository, InjectEntityManager } from '@nestjs/typeorm';
import { Performer } from 'entities/Performer.entity';
import { Song } from 'entities/Song.entity';
import { User } from 'entities/User.entity';
import { EntityManager, Repository, SelectQueryBuilder } from 'typeorm';
import {
  CreateSongDto,
  GetAllSongsResponseDto,
  GetAllSongsResponseItemDto,
  RawDbSong,
} from './song.type';

@Injectable()
export class SongService {
  constructor(
    @InjectEntityManager() private readonly entityManager: EntityManager,
    @InjectRepository(Song) private readonly songRepository: Repository<Song>,
    @InjectRepository(User) private readonly userRepository: Repository<User>,
    @InjectRepository(Performer)
    private readonly performerRepository: Repository<Performer>,
  ) {}

  async createSong(createSongDto: CreateSongDto, userDto: AuthUserDto) {
    // eslint-disable-next-line prefer-const
    let [user, performer] = await Promise.all([
      this.userRepository.findOne({ where: { id: userDto.id } }),
      this.performerRepository.findOne({
        where: { bandName: createSongDto.performerBandName },
      }),
    ]);
    const song = new Song();

    if (!performer) {
      performer = new Performer();
      performer.bandName = createSongDto.performerBandName;
      await this.performerRepository.save(performer);
    }

    song.chordPanels = createSongDto.chordPanels;
    song.rhythmPanels = createSongDto.rhythmPanels;
    song.titles = createSongDto.titles;
    song.rhythm = createSongDto.rhythm;
    song.songTitle = createSongDto.songTitle;
    song.views = 0;

    song.author = user;
    song.performer = performer;

    const savedSong = await this.songRepository.save(song);

    return savedSong;
  }

  async getAll(options: {
    top: number;
    skip: number;
    filter: string;
    orderBy: keyof Song | 'averageRating';
  }): Promise<GetAllSongsResponseDto> {
    const formattedOrderBy =
      options.orderBy === 'averageRating'
        ? '"averageRating"'
        : `song.${options.orderBy}`;

    const rawItems: RawDbSong[] = await this.entityManager.query(
      `
      SELECT "song"."id" AS "song_id",
        "song"."rhythmPanels" AS "song_rhythmPanels",
        "song"."chordPanels" AS "song_chordPanels",
        "song"."titles" AS "song_titles",
        "song"."rhythm" AS "song_rhythm",
        "song"."songTitle" AS "song_songTitle",
        "song"."views" AS "song_views",
        "song"."performerId" AS "song_performerId",
        "song"."authorId" AS "song_authorId",
        "performer"."id" AS "performer_id",
        "performer"."bandName" AS "performer_bandName",
        "author"."id" AS "author_id",
        "author"."username" AS "author_username",
        (COUNT(*) OVER())::INTEGER AS TOTALCOUNT,
        AVG("rating"."value") AS "averageRating"
      FROM (
        SELECT DISTINCT ON ("song"."songTitle", "song"."performerId")
        *
        FROM "song"
        ORDER BY 
          "song"."songTitle",
          "song"."performerId"
      ) "song"
      LEFT JOIN "rating" "rating" ON "rating"."songId" = "song"."id"
      INNER JOIN "performer" "performer" ON "performer"."id" = "song"."performerId"
      INNER JOIN "user" "author" ON "author"."id" = "song"."authorId"
      WHERE "song"."songTitle" ILIKE $1
      GROUP BY "song"."id",
        "performer"."id",
        "author"."id",
        "song"."rhythmPanels",
        "song"."chordPanels",
        "song"."titles",
        "song"."rhythm",
        "song"."songTitle",
        "song"."views",
        "song"."performerId",
        "song"."authorId"
      ORDER BY ${formattedOrderBy} DESC NULLS LAST
      LIMIT ${options.top}
      OFFSET ${options.skip}
     `,
      [`%${options.filter}%`],
    );

    const items = rawItems.map(
      (song): GetAllSongsResponseItemDto => ({
        id: song.song_id,
        rhythmPanels: song.song_rhythmPanels,
        chordPanels: song.song_chordPanels,
        titles: song.song_titles,
        rhythm: song.song_rhythm,
        songTitle: song.song_songTitle,
        views: song.song_views,
        performerId: song.performer_id,
        authorId: song.author_id,
        performer: {
          id: song.song_performerId,
          bandName: song.performer_bandName,
        },
        author: {
          id: song.song_authorId,
          username: song.author_username,
        },
        totalcount: song.totalcount,
        averageRating: song.averageRating,
      }),
    );
    const totalCount = rawItems[0]?.totalcount ?? 0;

    return { items, totalCount };
  }

  async getById(songId: number) {
    const [rating, song] = await Promise.all([
      this.songRepository
        .createQueryBuilder('song')
        .innerJoin('song.ratings', 'rating')
        .select('AVG(rating.value)::numeric(10, 1)', 'averageRating')
        .where('song.id = :songId', { songId })
        .groupBy('song.id')
        .getRawOne(),
      this.songRepository.findOne({
        where: { id: songId },
        relations: ['performer', 'author'],
      }),
    ]);

    if (!song) {
      throw new HttpException('Song not found', HttpStatus.NOT_FOUND);
    }
    return { ...song, ...rating };
  }

  async updateById(songId: number, songDto: CreateSongDto) {
    // eslint-disable-next-line prefer-const
    let [song, performer] = await Promise.all([
      this.songRepository.findOne({ where: { id: songId } }),
      this.performerRepository.findOne({
        where: { bandName: songDto.performerBandName },
      }),
    ]);

    if (!song) {
      throw new HttpException('Song not found', HttpStatus.NOT_FOUND);
    }

    if (!performer) {
      performer = new Performer();
      performer.bandName = songDto.performerBandName;
      await this.performerRepository.save(performer);
      song.performer = performer;
    }

    song.chordPanels = songDto.chordPanels;
    song.rhythmPanels = songDto.rhythmPanels;
    song.titles = songDto.titles;
    song.rhythm = songDto.rhythm;
    song.songTitle = songDto.songTitle;

    const savedSong = await this.songRepository.save(song);

    return savedSong;
  }

  async addViews(songId: number, viewsAmount: number) {
    const { affected } = await this.songRepository.increment(
      { id: songId },
      'views',
      viewsAmount,
    );

    if (affected < 1)
      throw new HttpException('Nothing is affected', HttpStatus.NOT_FOUND);

    return null;
  }

  async getSongVariants(songId: Song['id']) {
    const song = await this.songRepository.findOne({
      where: { id: songId },
      relations: ['performer'],
    });

    if (!song) {
      throw new HttpException('Song not found', HttpStatus.NOT_FOUND);
    }

    const { entities, raw } = await this.songRepository
      .createQueryBuilder('song')
      .leftJoin('song.ratings', 'rating')
      .innerJoinAndSelect('song.performer', 'performer')
      .innerJoinAndSelect('song.author', 'author')
      .addSelect('AVG(rating.value)', 'averageRating')
      .where('song.songTitle = :songTitle', { songTitle: song.songTitle })
      .andWhere('performer.bandName = :bandName', {
        bandName: song.performer.bandName,
      })
      .groupBy('song.id')
      .addGroupBy('performer.id')
      .addGroupBy('author.id')
      .orderBy('song.views', 'DESC', 'NULLS LAST')
      .addOrderBy('"averageRating"', 'DESC', 'NULLS LAST')
      .getRawAndEntities();

    const items = entities.map((song, songIndex) => ({
      ...song,
      averageRating: raw[songIndex].averageRating,
    }));

    return items;
  }
}
