import { Module } from '@nestjs/common';
import { SongService } from './song.service';
import { SongController } from './song.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Song } from 'entities/Song.entity';
import { User } from 'entities/User.entity';
import { Performer } from 'entities/Performer.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Song, User, Performer])],
  providers: [SongService],
  controllers: [SongController],
})
export class SongModule {}
