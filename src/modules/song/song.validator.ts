import * as Joi from '@hapi/joi';
import { ChordKeys } from 'types/Song.type';

const allowedKeys = Object.values(ChordKeys);

export const rhythmPanelsSchema = Joi.array().items(
  Joi.array().items(
    Joi.array().items(
      Joi.array().items(
        Joi.object({
          key: Joi.string().valid(...allowedKeys),
          body: Joi.string().empty(''),
          tonic: Joi.string().valid(...allowedKeys),
        }).allow(null),
      ),
    ),
  ),
);

export const chordPanelsSchema = Joi.array().items(
  Joi.array().items(
    Joi.array().items(
      Joi.object({
        word: Joi.string(),
        chordAlign: Joi.object({
          lineIndex: Joi.number(),
          tactIndex: Joi.number(),
          chordIndex: Joi.number(),
        }),
        id: Joi.string(),
      }),
    ),
  ),
);

export const titlesSchema = Joi.array().items(Joi.string());

export const rhythmSchema = Joi.array()
  .min(2)
  .max(2)
  .items(
    Joi.number()
      .min(1)
      .max(32),
  );

export const createSongSchema = Joi.object().keys({
  rhythmPanels: rhythmPanelsSchema.required(),
  chordPanels: chordPanelsSchema.required(),
  titles: titlesSchema.required(),
  rhythm: rhythmSchema.required(),
  performerBandName: Joi.string().required(),
  songTitle: Joi.string().required(),
});
