import { IsPositive, IsString } from 'class-validator';
import { Performer } from 'entities/Performer.entity';
import { Song } from 'entities/Song.entity';
import { User } from 'entities/User.entity';
import { ChordPanels, RhythmPanels } from '../../types/Song.type';

export class CreateSongDto {
  titles: string[];
  rhythmPanels: RhythmPanels;
  chordPanels: ChordPanels;
  rhythm: [number, number];
  performerBandName: string;
  songTitle: string;
}

export class GetSongsQuery {
  @IsPositive()
  top: number;

  @IsPositive()
  skip: number;

  @IsString()
  filter: string;
}

export interface GetAllSongsResponseItemDto
  extends Omit<Song, 'author' | 'ratings'> {
  totalcount: number;
  averageRating: number | null;
  performerId: Performer['id'];
  authorId: User['id'];
  performer: Pick<Performer, 'id' | 'bandName'>;
  author: Pick<User, 'id' | 'username'>;
}

export interface GetAllSongsResponseDto {
  items: GetAllSongsResponseItemDto[];
  totalCount: number;
}

export interface RawDbSong {
  song_id: Song['id'];
  song_rhythmPanels: Song['rhythmPanels'];
  song_chordPanels: Song['chordPanels'];
  song_titles: Song['titles'];
  song_rhythm: Song['rhythm'];
  song_songTitle: Song['songTitle'];
  song_views: Song['views'];
  song_performerId: Performer['id'];
  song_authorId: User['id'];
  performer_id: Performer['id'];
  performer_bandName: Performer['bandName'];
  author_id: User['id'];
  author_username: User['username'];
  totalcount: number;
  averageRating: number | null;
}
